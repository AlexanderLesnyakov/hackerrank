﻿using System;
using System.Linq;

namespace Hackerrank.Algorithms.Warmup
{
    /// <summary>
    ///     Solution for https://www.hackerrank.com/challenges/simple-array-sum
    /// </summary>
    public static class SimpleArraySum
    {
        public static string Solution(string arraySize, string elements)
        {
            if (string.IsNullOrWhiteSpace(arraySize))
                return string.Empty;

            var n = Convert.ToInt32(arraySize);
            if (n == 0)
                return string.Empty;

            var arrTemp = elements.Split(' ');
            var arr = Array.ConvertAll(arrTemp, int.Parse);
            return arr.Sum().ToString();
        }
    }
}