﻿using System;
using System.Globalization;

namespace Hackerrank.Algorithms.Warmup
{
    /// <summary>
    ///     Solution for https://www.hackerrank.com/challenges/plus-minus
    /// </summary>
    public static class PlusMinus
    {
        public static object Solution(string arraySize, string elements)
        {
            if (string.IsNullOrWhiteSpace(arraySize))
                return string.Empty;

            var n = Convert.ToInt32(arraySize);
            if (n == 0)
                return string.Empty;

            var arrTemp = elements.Split(' ');
            var arr = Array.ConvertAll(arrTemp, int.Parse);
            var less = 0;
            var zero = 0;
            var more = 0;
            foreach (var num in arr)
            {
                if (num < 0)
                    less++;
                else if (num == 0)
                    zero++;
                else
                    more++;
            }

            return ((float) more/n).ToString("F6", CultureInfo.InvariantCulture) + Environment.NewLine +
                   ((float) less/n).ToString("F6", CultureInfo.InvariantCulture) + Environment.NewLine +
                   ((float) zero/n).ToString("F6", CultureInfo.InvariantCulture);
        }
    }
}