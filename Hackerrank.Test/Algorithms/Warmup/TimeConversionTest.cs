﻿using Hackerrank.Algorithms.Warmup;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Hackerrank.Test.Algorithms.Warmup
{
    [TestClass]
    public class TimeConversionTest
    {
        [TestMethod]
        public void TestSolution()
        {
            const string time = "07:05:45PM";
            Assert.AreEqual("19:05:45", TimeConversion.Solution(time));
        }

        [TestMethod]
        public void TestSolutionEmptyInput()
        {
            const string time = "";
            Assert.AreEqual(string.Empty, TimeConversion.Solution(time));
        }
    }
}