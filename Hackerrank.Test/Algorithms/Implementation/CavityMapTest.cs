﻿using Hackerrank.Algorithms.Implementation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Hackerrank.Test.Algorithms.Implementation
{
    [TestClass]
    public class CavityMapTest
    {
        [TestMethod]
        public void TestSolution()
        {
            const string size = "4";
            const string elements =
@"1112
1912
1892
1234";
            const string expectedResult =
@"1112
1X12
18X2
1234";
            Assert.AreEqual(expectedResult, CavityMap.Solution(size, elements));
        }

        [TestMethod]
        public void TestSolutionZeroArraySize()
        {
            const string size = "0";
            const string elements = "";
            Assert.AreEqual(string.Empty, CavityMap.Solution(size, elements));
        }

        [TestMethod]
        public void TestSolutionEmptyArraySize()
        {
            const string size = "";
            const string elements = "";
            Assert.AreEqual(string.Empty, CavityMap.Solution(size, elements));
        }
    }
}